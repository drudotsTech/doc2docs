package com.drudotstech.doc2docs.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.EmptyModel;
import org.jetbrains.annotations.NotNull;
import java.util.List;

public class AllSurveyAdapter extends RecyclerView.Adapter<AllJobsAdapter.ViewHolder> {

    Context context;
    List<EmptyModel> allSurveyList;

    public AllSurveyAdapter(Context context, List<EmptyModel> allSurveyList) {
        this.context = context;
        this.allSurveyList = allSurveyList;
    }

    @NonNull
    @NotNull
    @Override
    public AllJobsAdapter.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_single_survey, parent, false);
        return new AllJobsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull AllJobsAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return allSurveyList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
        }
    }
}