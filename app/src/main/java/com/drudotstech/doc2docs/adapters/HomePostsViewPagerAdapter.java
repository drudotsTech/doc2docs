package com.drudotstech.doc2docs.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.EmptyModel;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HomePostsViewPagerAdapter extends RecyclerView.Adapter<HomePostsViewPagerAdapter.ViewHolder>{

    Context context;
    List<EmptyModel> imagesList;

    public HomePostsViewPagerAdapter(Context context, List<EmptyModel> imagesList) {
        this.context = context;
        this.imagesList = imagesList;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(context).inflate(R.layout.layout_post_content, parent, false);
       return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull HomePostsViewPagerAdapter.ViewHolder holder, int position) {
        EmptyModel image = imagesList.get(position);
        holder.post_Image.setImageResource(image.getImage());
    }

    @Override
    public int getItemCount() {
        return imagesList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        ImageView post_Image;
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            post_Image = itemView.findViewById(R.id.post_Image);
        }
    }
}
