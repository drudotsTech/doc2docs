package com.drudotstech.doc2docs.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager2.widget.ViewPager2;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.EmptyModel;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.List;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.ViewHolder> {

    private static final int VIEW_TYPE_STORIES = 0;
    private static final int VIEW_TYPE_SUGGESTED = 2;
    private static final int VIEW_TYPE_POST = 1;
    private static final int VIEW_TYPE_WEBINAR = 3;
    private static final int VIEW_TYPE_SURVEY = 4;

    Context context;
    List<EmptyModel> postsList;

    List<EmptyModel> suggestedUsers;
    List<EmptyModel> imagesList;


    public HomeAdapter(Context context, List<EmptyModel> postsList) {
        this.context = context;
        this.postsList = postsList;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view;
        if (viewType == VIEW_TYPE_STORIES) {
            view = LayoutInflater.from(context).inflate(R.layout.stories_layout, parent, false);
        }
        else if (viewType == VIEW_TYPE_SUGGESTED){
            view = LayoutInflater.from(context).inflate(R.layout.layout_suggestions, parent, false);
        }
        else if (viewType == VIEW_TYPE_POST){
            view = LayoutInflater.from(context).inflate(R.layout.layout_home_post_item, parent, false);
        }
        else if (viewType == VIEW_TYPE_WEBINAR){
            view = LayoutInflater.from(context).inflate(R.layout.layout_single_webinar_item, parent, false);
        }
        else{
            view = LayoutInflater.from(context).inflate(R.layout.layout_single_survey, parent, false);
        }

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull HomeAdapter.ViewHolder holder, int position) {
        if (position == 0){
            // WorkFor Stories Here
        }
        else if (position == 1){
            // show First Story Here
            if (postsList.get(0).getType().equals("post")){
                imagesList = new ArrayList<>();
                for (int i=0; i<2; i++){
                    imagesList.add(new EmptyModel(R.drawable.doctor));
                }
                showViewPagerItems(holder.text_imagesCounter, holder.viewPager2, imagesList);
            }

        }
        else if (position == 2){
            // show suggested usersList here
            suggestedUsers = new ArrayList<>();
            for (int i=0; i<10; i++){
                suggestedUsers.add(new EmptyModel());
            }
            showSuggestedUsers(holder.recyclerView_suggestedUsers, suggestedUsers);
        }
        else if (position > 2 && position < postsList.size()){
            if (postsList.get(position - 2).getType().equals("post")){
                imagesList = new ArrayList<>();
                for (int i=0; i<2; i++){
                    imagesList.add(new EmptyModel(R.drawable.doctor));
                }
                showViewPagerItems(holder.text_imagesCounter, holder.viewPager2, imagesList);
            }

        }

    }

    @Override
    public int getItemCount() {
        return postsList.size() + 2;
    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0){
            return VIEW_TYPE_STORIES;
        }
        else if (position == 1){
            if (postsList.get(0).getType().equals("post")){
                return VIEW_TYPE_POST;
            }
            else if (postsList.get(0).getType().equals("webinar")){
                return VIEW_TYPE_WEBINAR;
            }
            else {
                return VIEW_TYPE_SURVEY;
            }
        }
        else if (position == 2){
            return VIEW_TYPE_SUGGESTED;
        }
        else if (position > 2 && position < postsList.size()){
            if (postsList.get(position - 2).getType().equals("post")){
                return VIEW_TYPE_POST;
            }
            else if (postsList.get(position - 2).getType().equals("webinar")){
                return VIEW_TYPE_WEBINAR;
            }
            else {
                return VIEW_TYPE_SURVEY;
            }
        }
        else {
            return 10;
        }

    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        // Normal Post ;
        ViewPager2 viewPager2;
        TextView text_imagesCounter;

        //Suggested Users
        RecyclerView recyclerView_suggestedUsers;

        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
            // Normal Post ;
            viewPager2 = itemView.findViewById(R.id.viewPager2);
            text_imagesCounter = itemView.findViewById(R.id.text_imagesCounter);
            //Suggested Users
            recyclerView_suggestedUsers = itemView.findViewById(R.id.recyclerView_suggestedUsers);

        }
    }



    private void showSuggestedUsers(RecyclerView recyclerView, List<EmptyModel> suggestedUsers) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        SuggestedUsersAdapter suggestedUsersAdapter = new SuggestedUsersAdapter(context, suggestedUsers);
        recyclerView.setAdapter(suggestedUsersAdapter);
    }


    private void showViewPagerItems(TextView textView, ViewPager2 viewPager2, List<EmptyModel> imagesList) {
        HomePostsViewPagerAdapter imagesAdapter = new HomePostsViewPagerAdapter(context, imagesList);
        viewPager2.setAdapter(imagesAdapter);
        String sliderText = "1" + " / " + imagesList.size();
        if (imagesList.size() > 0){
            textView.setText(sliderText);
        }

        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                super.onPageScrolled(position, positionOffset, positionOffsetPixels);
                String sliderText = (position + 1) + " / " + imagesList.size();
                if (imagesList.size() > 0){
                    textView.setText(sliderText);
                }
            }
        });
    }
}
