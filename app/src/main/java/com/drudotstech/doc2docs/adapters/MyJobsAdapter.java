package com.drudotstech.doc2docs.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.EmptyModel;
import org.jetbrains.annotations.NotNull;
import java.util.List;

public class MyJobsAdapter extends RecyclerView.Adapter<MyJobsAdapter.ViewHolder> {

    Context context;
    List<EmptyModel> allJobsList;

    public MyJobsAdapter(Context context, List<EmptyModel> allJobsList) {
        this.context = context;
        this.allJobsList = allJobsList;
    }

    @NonNull
    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.layout_my_job_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return allJobsList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder{
        public ViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
        }
    }
}