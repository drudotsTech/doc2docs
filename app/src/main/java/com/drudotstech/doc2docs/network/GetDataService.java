package com.drudotstech.doc2docs.network;

import com.drudotstech.doc2docs.models.AuthResponseModel;
import com.drudotstech.doc2docs.models.MainResponseModel;
import com.drudotstech.doc2docs.models.OtpResponseModel;
import com.drudotstech.doc2docs.models.VerifyResponseModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface GetDataService {

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("user/auth")
    Call<AuthResponseModel> loginUser(@Field("loginName") String userEmail, @Field("loginPassword") String password, @Field("token") String token, @Field("device") String device, @Field("device_name") String device_name);



    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("user/auth")
    Call<AuthResponseModel> registerUser(@Field("user_name") String userName, @Field("name") String fullName,  @Field("email") String userEmail, @Field("phone") String userPhone, @Field("doc_id") String doc_id, @Field("specialization") String specialization, @Field("sub_spec") String sub_spec, @Field("password") String password, @Field("token") String token, @Field("device") String device, @Field("device_name") String device_name, @Field("country") String country);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("user/password/forgot")
    Call<OtpResponseModel> sendOtp(@Field("type") String type, @Field("email") String email);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("user/password/reset")
    Call<MainResponseModel> userPasswordReset(@Field("email") String userEmail, @Field("password") String password);

    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("user/password/forgot")
    Call<VerifyResponseModel> verifyUserEmail(@Field("type") String type, @Field("email") String userEmail,  @Field("user_name") String userName, @Field("phone") String phone);


    @Headers({
            "Accept: application/json",
    })
    @FormUrlEncoded
    @POST("user/password/forgot")
    Call<VerifyResponseModel> verifyDoctorId(@Field("type") String type, @Field("doctor_id") String doctor_id);
}
