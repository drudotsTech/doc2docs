package com.drudotstech.doc2docs.models;

import com.google.gson.annotations.SerializedName;

public class UserDataModel {
    @SerializedName("id")
    private int id;
    @SerializedName("user_name")
    private String userName;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("doctor_id")
    private String doctorId;
    @SerializedName("specialization")
    private String specialization;
    @SerializedName("email")
    private String email;
    @SerializedName("image")
    private String image;
    @SerializedName("flag")
    private String flag;
    @SerializedName("country")
    private String country;
    @SerializedName("phone")
    private String phone;
    @SerializedName("bio")
    private String bio;
    @SerializedName("location")
    private String location;
    @SerializedName("gender")
    private String gender;
    @SerializedName("platform")
    private String platform;
    @SerializedName("verified")
    private int verified;
    @SerializedName("follow")
    private boolean follow;

    public UserDataModel() {
    }

    public UserDataModel(int id, String userName, String firstName, String lastName, String doctorId, String specialization, String email, String image, String flag, String country, String phone, String bio, String location, String gender, String platform, int verified, boolean follow) {
        this.id = id;
        this.userName = userName;
        this.firstName = firstName;
        this.lastName = lastName;
        this.doctorId = doctorId;
        this.specialization = specialization;
        this.email = email;
        this.image = image;
        this.flag = flag;
        this.country = country;
        this.phone = phone;
        this.bio = bio;
        this.location = location;
        this.gender = gender;
        this.platform = platform;
        this.verified = verified;
        this.follow = follow;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public int getVerified() {
        return verified;
    }

    public void setVerified(int verified) {
        this.verified = verified;
    }


    public boolean isFollow() {
        return follow;
    }

    public void setFollow(boolean follow) {
        this.follow = follow;
    }
}
