package com.drudotstech.doc2docs.models;

import com.google.gson.annotations.SerializedName;

public class OtpResponseModel {
    @SerializedName("status")
    private boolean status;
    @SerializedName("data")
    private int otp;
    @SerializedName("action")
    private String action;

    public OtpResponseModel() {
    }

    public OtpResponseModel(boolean status, int otp, String action) {
        this.status = status;
        this.otp = otp;
        this.action = action;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getOtp() {
        return otp;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
