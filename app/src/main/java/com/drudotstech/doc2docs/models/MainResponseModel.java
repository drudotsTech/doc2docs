package com.drudotstech.doc2docs.models;

import com.google.gson.annotations.SerializedName;

public class MainResponseModel {
    @SerializedName("status")
    private boolean status;
    @SerializedName("action")
    private String action;

    public MainResponseModel() { }

    public MainResponseModel(boolean status, String action) {
        this.status = status;
        this.action = action;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
