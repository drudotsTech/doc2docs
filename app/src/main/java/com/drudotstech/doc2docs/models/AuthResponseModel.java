package com.drudotstech.doc2docs.models;

import com.google.gson.annotations.SerializedName;

public class AuthResponseModel {

    @SerializedName("status")
    private boolean status;
    @SerializedName("data")
    private UserDataModel userData;
    @SerializedName("action")
    private String action;

    public AuthResponseModel() {
    }

    public AuthResponseModel(boolean status, UserDataModel userData, String action) {
        this.status = status;
        this.userData = userData;
        this.action = action;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public UserDataModel getUserData() {
        return userData;
    }

    public void setUserData(UserDataModel userData) {
        this.userData = userData;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
