package com.drudotstech.doc2docs.models;

public class EmptyModel {
    private int Image;
    private String type;

    public EmptyModel() { }

    public EmptyModel(int image) {
        Image = image;
    }

    public EmptyModel(String type) {
        this.type = type;
    }

    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
