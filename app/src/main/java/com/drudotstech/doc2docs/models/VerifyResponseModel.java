package com.drudotstech.doc2docs.models;


import com.google.gson.annotations.SerializedName;

public class VerifyResponseModel {
    @SerializedName("status")
    private boolean status;
    @SerializedName("type")
    private String type;

    public VerifyResponseModel() {
    }

    public VerifyResponseModel(boolean status, String type) {
        this.status = status;
        this.type = type;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
