package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.AuthResponseModel;
import com.drudotstech.doc2docs.models.UserDataModel;
import com.drudotstech.doc2docs.network.GetDataService;
import com.drudotstech.doc2docs.network.RetrofitClientInstance;
import com.drudotstech.doc2docs.utilities.CustomCookieToast;
import com.drudotstech.doc2docs.utilities.Utilities;
import com.wang.avi.AVLoadingIndicatorView;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {



    GetDataService service;
    AVLoadingIndicatorView progressLoading;
    View shadow_View;
    Context context;

    TextView text_ForgotPassword, text_SignUp;
    Button btn_login;

    EditText edit_userEmail, edit_textPassword;


    boolean isPasswordShown = false;
    ImageView showHide_ic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        context = LoginActivity.this;
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        progressLoading = findViewById(R.id.loading);
        shadow_View = findViewById(R.id.shadow_View);

        text_ForgotPassword = findViewById(R.id.text_ForgotPassword);
        text_SignUp = findViewById(R.id.text_SignUp);
        btn_login = findViewById(R.id.btn_login);
        edit_userEmail = findViewById(R.id.edit_userEmail);
        edit_textPassword = findViewById(R.id.edit_textPassword);
        showHide_ic = findViewById(R.id.showHide_ic);


        text_ForgotPassword.setOnClickListener(v -> {
            Intent mainIntent = new Intent(context, ForgotPasswordActivity.class);
            startActivity(mainIntent);
            overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
        });

        text_SignUp.setOnClickListener(v -> {
            Intent mainIntent = new Intent(context, SignUpActivity.class);
            startActivity(mainIntent);
            overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
        });

        btn_login.setOnClickListener(v -> {
            String userEmail = edit_userEmail.getText().toString();
            String password = edit_textPassword.getText().toString();
            if (userEmail.isEmpty()) {
                CustomCookieToast.showRequiredToast(LoginActivity.this,"Please enter your phone number, email or username");
            } else if (password.isEmpty()) {
                CustomCookieToast.showRequiredToast(LoginActivity.this,"Please enter your password");
            }
            else {
                shadow_View.setVisibility(View.VISIBLE);
                progressLoading.setVisibility(View.VISIBLE);
                progressLoading.show();
                userLogin(userEmail, password);
            }
        });

        showHide_ic.setOnClickListener(v -> {
            if (!isPasswordShown){
                showHide_ic.setImageResource(R.drawable.show_ic);
                isPasswordShown = true;
                edit_textPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else {
                showHide_ic.setImageResource(R.drawable.hide_ic);
                isPasswordShown = false;
                edit_textPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });

    }


    private void userLogin(String userEmail, String userPassword) {
        Call<AuthResponseModel> call = service.loginUser(userEmail, userPassword, "device_token", "device_id", "android");
        call.enqueue(new Callback<AuthResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<AuthResponseModel> call, @NotNull Response<AuthResponseModel> response) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    boolean status = response.body().isStatus();
                    if (status) {
                        // Working with response and saving data in Shared Preferences
                        UserDataModel user = response.body().getUserData();
                        int userId = user.getId();
                        String userName = user.getUserName();
                        String firstName = user.getFirstName();
                        String lastName = user.getLastName();
                        String userEmail = user.getEmail();
                        String userPhone = user.getPhone();
                        String profile_image = user.getImage();
                        String userGender = user.getGender();
                        String userBio = user.getBio();
                        String userPlatform = user.getPlatform();
                        String userLocation = user.getLocation();
                        String userSpecialization = user.getSpecialization();

                        Utilities.saveInt(context, "user_Id", userId);
                        Utilities.saveString(context, "user_Name", userName);
                        Utilities.saveString(context, "user_firstName", firstName);
                        Utilities.saveString(context, "user_lastName", lastName);
                        Utilities.saveString(context, "user_Email", userEmail);
                        Utilities.saveString(context, "user_Phone", userPhone);
                        Utilities.saveString(context, "user_Profile", profile_image);
                        Utilities.saveString(context, "user_Gender", userGender);
                        Utilities.saveString(context, "user_Bio", userBio);
                        Utilities.saveString(context, "user_Platform", userPlatform);
                        Utilities.saveString(context, "user_Specialization", userSpecialization);
                        Utilities.saveString(context, "user_Location", userLocation);
                        Utilities.saveString(context, "is_LoggedIn", "true");
                        Utilities.saveString(context, "user_Password", userPassword);

                        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
                        finish();
                    } else {
                        CustomCookieToast.showFailureToast(LoginActivity.this,"Error!", response.body().getAction());
                    }
                } else {
                    CustomCookieToast.showFailureToast(LoginActivity.this, "Error!", response.message());
                }

            }

            @Override
            public void onFailure(@NotNull Call<AuthResponseModel> call, @NotNull Throwable t) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                CustomCookieToast.showFailureToast(LoginActivity.this, t.getMessage());
            }
        });
    }


}