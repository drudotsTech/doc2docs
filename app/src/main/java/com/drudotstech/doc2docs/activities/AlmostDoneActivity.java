package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.AuthResponseModel;
import com.drudotstech.doc2docs.models.UserDataModel;
import com.drudotstech.doc2docs.network.GetDataService;
import com.drudotstech.doc2docs.network.RetrofitClientInstance;
import com.drudotstech.doc2docs.utilities.CustomCookieToast;
import com.drudotstech.doc2docs.utilities.Utilities;
import com.wang.avi.AVLoadingIndicatorView;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AlmostDoneActivity extends AppCompatActivity {

    Button btn_SignUp, btn_Back;

    GetDataService service;
    AVLoadingIndicatorView progressLoading;
    View shadow_View;
    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_almost_done);

        context  = AlmostDoneActivity.this;
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        progressLoading = findViewById(R.id.loading);
        shadow_View = findViewById(R.id.shadow_View);


        String userName = Utilities.getString(context, "user_Name");
        String user_fullName = Utilities.getString(context, "user_fullName");
        String userEmail = Utilities.getString(context, "user_Email");
        String userPhone = Utilities.getString(context, "user_Phone");
        String doctorId = Utilities.getString(context, "user_doctorId");
        String specialization = Utilities.getString(context, "user_specialization");
        String subSpecialization = Utilities.getString(context, "user_subSpecialization");
        String countryCode = Utilities.getString(context, "user_countryCode");
        String userPassword = Utilities.getString(context, "user_Password");


        btn_SignUp = findViewById(R.id.btn_SignUp);
        btn_Back = findViewById(R.id.btn_Back);


        btn_SignUp.setOnClickListener(v -> {
            shadow_View.setVisibility(View.VISIBLE);
            progressLoading.setVisibility(View.VISIBLE);
            progressLoading.show();
            requestForRegister(userName, user_fullName, userEmail, userPhone, doctorId, specialization, subSpecialization,userPassword, countryCode );
        });

        btn_Back.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
        });
    }



    private void requestForRegister(String userName, String userFullName, String userEmail, String userPhone, String userDoctorId, String userSpecialization, String user_subSpecialization, String userPassword, String countryCode){
        Call<AuthResponseModel> call = service.registerUser(userName, userFullName, userEmail, userPhone, userDoctorId, userSpecialization,user_subSpecialization, userPassword, "2jowjsc", "fewgw", "android", countryCode);
        call.enqueue(new Callback<AuthResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<AuthResponseModel> call, @NotNull Response<AuthResponseModel> response) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                if (response.isSuccessful()){
                    assert response.body() != null;
                    boolean status  = response.body().isStatus();
                    if (status){
                        // Working with response and saving data in Shared Preferences
                        UserDataModel user =  response.body().getUserData();
                        int userId = user.getId();
                        String userName = user.getUserName();
                        String userEmail = user.getEmail();
                        String userPhone = user.getPhone();
                        String profile_image = user.getImage();
                        String userGender = user.getGender();
                        String userBio = user.getBio();
                        String userPlatform = user.getPlatform();

                        String userLocation =  user.getLocation();
                        String userSpecialization =  user.getSpecialization();

                        Utilities.saveInt(context, "user_Id", userId);
                        Utilities.saveString(context, "user_Name", userName);

                        Utilities.saveString(context, "user_Specialization", userSpecialization);
                        Utilities.saveString(context, "user_Email", userEmail);
                        Utilities.saveString(context, "user_Phone", userPhone);
                        Utilities.saveString(context, "user_Profile", profile_image);
                        Utilities.saveString(context, "user_Gender", userGender);
                        Utilities.saveString(context, "user_Bio", userBio);
                        Utilities.saveString(context, "user_Platform", userPlatform);
                        Utilities.saveString(context, "user_Location", userLocation);
                        Utilities.saveString(context, "is_LoggedIn", "true");
                        Utilities.saveString(context, "user_Password", userPassword);

                        finishAffinity();
                        Intent mainIntent =  new Intent(context, HomeActivity.class);
                        startActivity(mainIntent);
                        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
                    }
                    else {
                        CustomCookieToast.showFailureToast(AlmostDoneActivity.this,"Error!", response.body().getAction());
                    }
                }
                else
                {
                    CustomCookieToast.showFailureToast(AlmostDoneActivity.this,"Error!", response.message());
                }


            }

            @Override
            public void onFailure(@NotNull Call<AuthResponseModel> call, @NotNull Throwable t) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                CustomCookieToast.showFailureToast(AlmostDoneActivity.this,"Error!", t.getMessage());
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }
}