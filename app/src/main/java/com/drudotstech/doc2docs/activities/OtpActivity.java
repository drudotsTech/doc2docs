package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.OtpResponseModel;
import com.drudotstech.doc2docs.network.GetDataService;
import com.drudotstech.doc2docs.network.RetrofitClientInstance;
import com.drudotstech.doc2docs.utilities.CustomCookieToast;
import com.drudotstech.doc2docs.utilities.Utilities;
import com.wang.avi.AVLoadingIndicatorView;

import org.jetbrains.annotations.NotNull;

import in.aabhasjindal.otptextview.OtpTextView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OtpActivity extends AppCompatActivity {

    GetDataService service;
    AVLoadingIndicatorView progressLoading;
    View shadow_View;
    Context context;

    Button btn_Next, btn_Back;
    OtpTextView otp_view;
    TextView text_userEmail, btn_Resend;
    String otp, userEmail;

    String text_Resend = "Resend Code!";
    String text_areYouSure = "Are you sure you want to resend code?";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        context = OtpActivity.this;

        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        progressLoading = findViewById(R.id.loading);
        shadow_View = findViewById(R.id.shadow_View);

        btn_Next = findViewById(R.id.btn_Next);
        text_userEmail = findViewById(R.id.text_userEmail);
        otp_view = findViewById(R.id.otp_view);
        btn_Resend = findViewById(R.id.btn_Resend);
        btn_Back = findViewById(R.id.btn_Back);

        otp = Utilities.getString(context, "otp");
        userEmail =  Utilities.getString(context, "user_Email");
        text_userEmail.setText(userEmail);

        btn_Next.setOnClickListener(v -> {
            String userOtp = otp_view.getOTP();
            if (userOtp.length() !=6){
                CustomCookieToast.showRequiredToast(OtpActivity.this,"Please enter otp code");
            }
            else if (! userOtp.equals(otp)){
                CustomCookieToast.showRequiredToast(OtpActivity.this,"Your code invalid.Try again!");
            }
            else {
              Intent forgotIntent = new Intent(OtpActivity.this, ResetPasswordActivity.class);
                startActivity(forgotIntent);
                overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
            }
        });

        btn_Resend.setOnClickListener(v -> doYouWantToResendCodePopUp());

        btn_Back.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
        });
    }


    public void doYouWantToResendCodePopUp(){

        AlertDialog reportPostPopup;
        AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.CustomDialog);
        final View customLayout =  LayoutInflater.from(context).inflate(R.layout.popup_logout_user, null);
        builder.setView(customLayout);

        TextView text_blockThisPerson = customLayout.findViewById(R.id.text_blockThisPerson);
        TextView text_sure = customLayout.findViewById(R.id.text_sure);
        text_blockThisPerson.setText(text_Resend);
        text_sure.setText(text_areYouSure);
        TextView btn_yes = customLayout.findViewById(R.id.text_Yes);
        TextView btn_cancel = customLayout.findViewById(R.id.text_No);
        reportPostPopup = builder.create();
        reportPostPopup.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        reportPostPopup.show();
        reportPostPopup.setCancelable(false);
        btn_cancel.setOnClickListener(v -> reportPostPopup.dismiss());

        btn_yes.setOnClickListener(v -> {
            // Clear Otp and resend code again
            reportPostPopup.dismiss();
            otp_view.setOTP("");
            shadow_View.setVisibility(View.VISIBLE);
            progressLoading.show();
            resendCode(userEmail);
        });
    }

    private void resendCode(String userEmail){
        Call<OtpResponseModel> call = service.sendOtp("email", userEmail);
        call.enqueue(new Callback<OtpResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<OtpResponseModel> call, @NotNull Response<OtpResponseModel> response) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                if (response.isSuccessful()){
                    assert response.body() != null;
                    boolean status  = response.body().isStatus();
                    if (status){
                        otp = String.valueOf(response.body().getOtp());
                        Utilities.saveString(context, "otp", otp);
                        CustomCookieToast.showSuccessToast(OtpActivity.this,"Code Send Successfully!", "Please check your email");
                    }
                    else {
                        CustomCookieToast.showFailureToast(OtpActivity.this,"Error!", response.body().getAction());
                    }
                }
                else {
                    CustomCookieToast.showFailureToast(OtpActivity.this,"Error!", response.message());
                }

            }

            @Override
            public void onFailure(@NotNull Call<OtpResponseModel> call, @NotNull Throwable t) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                CustomCookieToast.showFailureToast(OtpActivity.this,"Error!", t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }
}