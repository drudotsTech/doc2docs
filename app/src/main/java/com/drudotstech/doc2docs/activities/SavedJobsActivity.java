package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.os.Bundle;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.adapters.AllJobsAdapter;
import com.drudotstech.doc2docs.models.EmptyModel;
import java.util.ArrayList;
import java.util.List;

public class SavedJobsActivity extends AppCompatActivity {
    Context context;

    RecyclerView recyclerView_savedJobs;
    List<EmptyModel> savedJobsList;
    AllJobsAdapter savedJobsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_jobs);

        context = SavedJobsActivity.this;

        recyclerView_savedJobs = findViewById(R.id.recyclerView_savedJobs);

        savedJobsList = new ArrayList<>();

        for (int i=0; i<10; i++){
            savedJobsList.add(new EmptyModel());
        }

        showSavedJobsRecyclerView(recyclerView_savedJobs, savedJobsList);
    }


    private void showSavedJobsRecyclerView(RecyclerView recyclerView, List<EmptyModel> listItems) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        savedJobsAdapter = new AllJobsAdapter(context, listItems);
        recyclerView.setAdapter(savedJobsAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }
}