package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;

import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.adapters.AllSurveyAdapter;
import com.drudotstech.doc2docs.models.EmptyModel;

import java.util.ArrayList;
import java.util.List;

public class MyClinicalStudyActivity extends AppCompatActivity {
    Context context;

    RecyclerView recyclerView_Surveys;
    List<EmptyModel> listItems;
    AllSurveyAdapter allSurveyAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_clinical_study);

        context = MyClinicalStudyActivity.this;

        recyclerView_Surveys = findViewById(R.id.recyclerView_Surveys);
        listItems = new ArrayList<>();

        for (int i =0; i< 10; i++){
            listItems.add(new EmptyModel());
        }

        showAllSurveyRecyclerView(recyclerView_Surveys, listItems);


    }

    private void showAllSurveyRecyclerView(RecyclerView recyclerView, List<EmptyModel> allSurveyList) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        allSurveyAdapter = new AllSurveyAdapter(context, allSurveyList);
        recyclerView.setAdapter(allSurveyAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }
}