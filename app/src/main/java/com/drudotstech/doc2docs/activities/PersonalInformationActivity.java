package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.OtpResponseModel;
import com.drudotstech.doc2docs.models.VerifyResponseModel;
import com.drudotstech.doc2docs.network.GetDataService;
import com.drudotstech.doc2docs.network.RetrofitClientInstance;
import com.drudotstech.doc2docs.utilities.CustomCookieToast;
import com.drudotstech.doc2docs.utilities.Utilities;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.wang.avi.AVLoadingIndicatorView;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PersonalInformationActivity extends AppCompatActivity {

    Button btn_Next, btn_Back;

    EditText edit_doctorFullName, edit_doctorId;
    MaterialSpinner specialization_spinner, SubSpecialization_spinner;

    String[] specializationList = {"Ophtalmology"};
    String[] ophtalmologySubCatergoies = {"Retina médica ", "Cirugía vítreo retina", "Glaucoma", "Superficie ocular y córnea", "Cirugía facorefractiva o cataratas", "Cirugía refractiva", "Pediátrica", "Uveítis", "Estrabismo", "Cirugía plástica ocular y orbitaria (oculoplastia)", "Neurooftalmología", "Oftalmología general"};
    String specialization = "" ;
    String subSpecialization = "" ;

    GetDataService service;
    AVLoadingIndicatorView progressLoading;
    View shadow_View;
    Context context;



    String user_Name, user_Email, userPassword, userPhone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_information);

        context  = PersonalInformationActivity.this;
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        progressLoading = findViewById(R.id.loading);
        shadow_View = findViewById(R.id.shadow_View);


        btn_Next = findViewById(R.id.btn_Next);
        edit_doctorFullName = findViewById(R.id.edit_doctorFullName);
        edit_doctorId = findViewById(R.id.edit_doctorId);
        specialization_spinner = findViewById(R.id.specialization_spinner);
        SubSpecialization_spinner = findViewById(R.id.SubSpecialization_spinner);
        btn_Back = findViewById(R.id.btn_Back);


        specialization_spinner.setItems(specializationList);

        specialization_spinner.setOnItemSelectedListener((view, position, id, item) -> {
            specialization = item.toString();
            if (specialization.equals("Ophtalmology")){
                SubSpecialization_spinner.setItems(ophtalmologySubCatergoies);
            }
        });




        SubSpecialization_spinner.setOnItemSelectedListener((view, position, id, item) -> {
            if (specialization == null || specialization.isEmpty()){
              CustomCookieToast.showRequiredToast(PersonalInformationActivity.this, "Please select specialization");
            }
            else {
                subSpecialization = item.toString();
            }
        });

        user_Name = Utilities.getString(context, "user_Name");
        user_Email = Utilities.getString(context, "user_Email");
        userPassword = Utilities.getString(context, "user_Password");
        userPhone = Utilities.getString(context, "user_Phone");

        btn_Next.setOnClickListener(v -> {
            String fullName = edit_doctorFullName.getText().toString();
            String doctorId = edit_doctorId.getText().toString();
            if (fullName.isEmpty()){
                CustomCookieToast.showRequiredToast(PersonalInformationActivity.this,"Please enter your fullname");
            }
            else if (doctorId.isEmpty()){
                CustomCookieToast.showRequiredToast(PersonalInformationActivity.this,"Please enter your doctor ID");
            }
            else if (specialization.isEmpty()){
                CustomCookieToast.showRequiredToast(PersonalInformationActivity.this,"Please select your specialization");
            }
            else if (subSpecialization.isEmpty()){
                CustomCookieToast.showRequiredToast(PersonalInformationActivity.this,"Please select your sub-specialization");
            }
            else {
                shadow_View.setVisibility(View.VISIBLE);
                progressLoading.setVisibility(View.VISIBLE);
                progressLoading.show();
                confirmUserIdAndSendOtp(user_Email, fullName, doctorId, specialization, subSpecialization);
            }
        });

        btn_Back.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
        });
    }

    private void confirmUserIdAndSendOtp(String userEmail, String fullName, String doctorId, String specialization, String subSpecialization){
        Call<VerifyResponseModel> verifyDoctorId = service.verifyDoctorId("verify", doctorId);
        verifyDoctorId.enqueue(new Callback<VerifyResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<VerifyResponseModel> call, @NotNull Response<VerifyResponseModel> response) {
                if (response.isSuccessful()){
                    assert response.body() != null;
                    if (response.body().isStatus()){
                            Call<OtpResponseModel> sendOtpCall = service.sendOtp("send_code", userEmail);
                            sendOtpCall.enqueue(new Callback<OtpResponseModel>() {
                                @Override
                                public void onResponse(@NotNull Call<OtpResponseModel> call, @NotNull Response<OtpResponseModel> response) {
                                    shadow_View.setVisibility(View.GONE);
                                    progressLoading.hide();
                                    assert response.body() != null;
                                    boolean otpStatus  = response.body().isStatus();
                                    if (otpStatus){
                                        // Save all the required data here
                                        Utilities.saveString(context, "otp", String.valueOf(response.body().getOtp()));
                                        Utilities.saveString(context, "user_fullName", fullName);
                                        Utilities.saveString(context, "user_doctorId", doctorId);
                                        Utilities.saveString(context, "user_specialization", specialization);
                                        Utilities.saveString(context, "user_Name", user_Name);
                                        Utilities.saveString(context, "user_Email", userEmail);
                                        Utilities.saveString(context, "user_Phone", userPhone);
                                        Utilities.saveString(context, "user_doctorId", doctorId);
                                        Utilities.saveString(context, "user_subSpecialization", subSpecialization);
                                        Utilities.saveString(context, "user_Password", userPassword);
                                        Intent i = new Intent(context , ProfileVerificationActivity.class);
                                        startActivity(i);
                                        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
                                    }
                                    else {
                                        shadow_View.setVisibility(View.GONE);
                                        progressLoading.hide();
                                        CustomCookieToast.showFailureToast(PersonalInformationActivity.this,"Error!", response.body().getAction());
                                    }
                                }

                                @Override
                                public void onFailure(@NotNull Call<OtpResponseModel> call, @NotNull Throwable t) {
                                    shadow_View.setVisibility(View.GONE);
                                    progressLoading.hide();
                                    CustomCookieToast.showFailureToast(PersonalInformationActivity.this,"Error!", t.getMessage());
                                }
                            });
                        }
                    }
                    else {
                        shadow_View.setVisibility(View.GONE);
                        progressLoading.hide();
                        CustomCookieToast.showFailureToast(PersonalInformationActivity.this,"Doctor ID Error!", "Your doctor ID already exist please enter unique doctor ID");
                    }
                }

            @Override
            public void onFailure(@NotNull Call<VerifyResponseModel> call, @NotNull Throwable t) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                CustomCookieToast.showFailureToast(PersonalInformationActivity.this,"Error!", t.getMessage());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }
}