package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.utilities.Utilities;

public class SplashActivity extends AppCompatActivity {

    Context context;

    String is_LoggedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = SplashActivity.this;

        is_LoggedIn = Utilities.getString(context, "is_LoggedIn");

        if (is_LoggedIn.equals("true")) {
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                Intent mainIntent = new Intent(context, HomeActivity.class);
                startActivity(mainIntent);
                overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
                finish();
            }, 500);
        }
        else {
            new Handler(Looper.getMainLooper()).postDelayed(() -> {
                Intent mainIntent = new Intent(context, LoginActivity.class);
                startActivity(mainIntent);
                overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
                finish();
            }, 500);
        }

    }
}