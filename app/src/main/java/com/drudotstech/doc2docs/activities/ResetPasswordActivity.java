package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.MainResponseModel;
import com.drudotstech.doc2docs.network.GetDataService;
import com.drudotstech.doc2docs.network.RetrofitClientInstance;
import com.drudotstech.doc2docs.utilities.CustomCookieToast;
import com.drudotstech.doc2docs.utilities.Utilities;
import com.wang.avi.AVLoadingIndicatorView;
import org.jetbrains.annotations.NotNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResetPasswordActivity extends AppCompatActivity {
    GetDataService service;
    AVLoadingIndicatorView progressLoading;
    View shadow_View;
    Context context;
    String userEmail;


    EditText edit_newPassword, edit_confirmPassword;

    Button btn_Reset;


    ImageView showHide_ic, confirm_showHide_ic;
    boolean isPasswordShown, confirmPasswordShown= false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        context  = ResetPasswordActivity.this;
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        progressLoading = findViewById(R.id.loading);
        shadow_View = findViewById(R.id.shadow_View);
        userEmail = Utilities.getString(context, "user_Email");

        edit_newPassword = findViewById(R.id.edit_newPassword);
        edit_confirmPassword = findViewById(R.id.edit_confirmPassword);
        btn_Reset = findViewById(R.id.btn_Reset);

        showHide_ic = findViewById(R.id.showHide_ic);
        confirm_showHide_ic = findViewById(R.id.confirm_showHide_ic);

        btn_Reset.setOnClickListener(view -> {
            String userPassword = edit_newPassword.getText().toString();
            String confirmPassword = edit_confirmPassword.getText().toString();
            if (userPassword.isEmpty())
            {
                CustomCookieToast.showRequiredToast(ResetPasswordActivity.this,"Please enter new password");
            }
            else if (confirmPassword.isEmpty()){
                CustomCookieToast.showRequiredToast(ResetPasswordActivity.this,"Please enter confirm password");
            }
            else if (userPassword.length() < 6){
                CustomCookieToast.showRequiredToast(ResetPasswordActivity.this,"Please enter 6 digit password");
            }
            else if (confirmPassword.length() < 6){
                CustomCookieToast.showRequiredToast(ResetPasswordActivity.this,"Please enter 6 digit confirm password");
            }
            else if (! userPassword.equals(confirmPassword)){
                CustomCookieToast.showRequiredToast(ResetPasswordActivity.this,"Please enter match password");
            }
            else {
                shadow_View.setVisibility(View.VISIBLE);
                progressLoading.setVisibility(View.VISIBLE);
                progressLoading.show();
                requestForReset(userEmail, userPassword);
            }
        });



        showHide_ic.setOnClickListener(v -> {
            if (!isPasswordShown){
                showHide_ic.setImageResource(R.drawable.show_ic);
                isPasswordShown = true;
                edit_newPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else {
                showHide_ic.setImageResource(R.drawable.hide_ic);
                isPasswordShown = false;
                edit_newPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });

        confirm_showHide_ic.setOnClickListener(v -> {
            if (!confirmPasswordShown){
                confirm_showHide_ic.setImageResource(R.drawable.show_ic);
                confirmPasswordShown = true;
                edit_confirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else {
                confirm_showHide_ic.setImageResource(R.drawable.hide_ic);
                confirmPasswordShown = false;
                edit_confirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });
    }


    private void requestForReset(String userEmail, String userPassword){
        Call<MainResponseModel> call = service.userPasswordReset(userEmail, userPassword);
        call.enqueue(new Callback<MainResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<MainResponseModel> call, @NotNull Response<MainResponseModel> response) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                if (response.isSuccessful()){
                    assert response.body() != null;
                    boolean status  = response.body().isStatus();
                    if (status){
                        CustomCookieToast.showSuccessToast(ResetPasswordActivity.this,response.body().getAction());

                        new Handler(Looper.getMainLooper()).postDelayed(() -> {
                            finishAffinity();
                            Intent mainIntent =  new Intent(context, LoginActivity.class);
                            startActivity(mainIntent);
                            overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
                        }, 1000);


                    }
                    else {
                        CustomCookieToast.showFailureToast(ResetPasswordActivity.this,"Error!" ,response.body().getAction());
                    }

                }
                else {
                    CustomCookieToast.showFailureToast(ResetPasswordActivity.this,"Error!" ,response.message());
                }


            }

            @Override
            public void onFailure(@NotNull Call<MainResponseModel> call, @NotNull Throwable t) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                CustomCookieToast.showFailureToast(ResetPasswordActivity.this,"Error!", t.getMessage());
            }
        });
    }
}