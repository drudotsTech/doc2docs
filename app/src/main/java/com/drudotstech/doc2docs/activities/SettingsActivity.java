package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.drudotstech.doc2docs.R;

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }
}