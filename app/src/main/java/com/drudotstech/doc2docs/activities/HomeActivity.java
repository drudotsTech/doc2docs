package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.adapters.ViewPagerFragmentAdapter;
import com.drudotstech.doc2docs.fragments.CoursesFragment;
import com.drudotstech.doc2docs.fragments.HomeFragment;
import com.drudotstech.doc2docs.fragments.MenuFragment;
import com.drudotstech.doc2docs.fragments.NotificationFragment;
import com.drudotstech.doc2docs.fragments.WebinarFragment;
import java.util.ArrayList;
import java.util.List;

public class HomeActivity extends AppCompatActivity{

    Context context;

    ViewPager2 viewPager2;
    RelativeLayout layout_home, layout_notification, layout_courses, layout_webinar, layout_settings;
    ImageView home_ic, notification_ic, courses_ic, webinar_ic, menu_ic;

    List<Fragment> fragmentsList;
    ViewPagerFragmentAdapter myAdapter;

    int current = 0;
    private boolean pressedBackOnce = false;

    Fragment homeFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        context = HomeActivity.this;

        boolean finish = getIntent().getBooleanExtra("finish", false);
        if (finish) {
            startActivity(new Intent(HomeActivity.this, LoginActivity.class));
            finish();
        }


        viewPager2 = findViewById(R.id.viewPager2);
        layout_home = findViewById(R.id.layout_home);
        home_ic = findViewById(R.id.home_ic);
        layout_notification = findViewById(R.id.layout_notification);
        notification_ic = findViewById(R.id.notification_ic);
        layout_courses = findViewById(R.id.layout_courses);
        courses_ic = findViewById(R.id.courses_ic);
        layout_webinar = findViewById(R.id.layout_webinar);
        webinar_ic = findViewById(R.id.webinar_ic);
        layout_settings = findViewById(R.id.layout_settings);
        menu_ic = findViewById(R.id.menu_ic);

        fragmentsList = new ArrayList<>();
        homeFragment = new HomeFragment();
        fragmentsList.add(homeFragment);
        fragmentsList.add(new NotificationFragment());
        fragmentsList.add(new CoursesFragment());
        fragmentsList.add(new WebinarFragment());
        fragmentsList.add(new MenuFragment());

        myAdapter = new ViewPagerFragmentAdapter(HomeActivity.this , fragmentsList);
        viewPager2.setUserInputEnabled(false);
        viewPager2.setAdapter(myAdapter);
        viewPager2.setOffscreenPageLimit(5);

        setDrawableIcon(0);

        layout_home.setOnClickListener(v -> {
            if (current != 0) {
                viewPager2.setCurrentItem(0, false);
                current = 0;
                setDrawableIcon(0);
                pressedBackOnce = true;
            }
            else {
                ((HomeFragment) homeFragment).scrollToTop();
            }
        });

        layout_notification.setOnClickListener(v -> {
            if (current != 1) {
                viewPager2.setCurrentItem(1, false);
                current = 1;
                setDrawableIcon(1);
                pressedBackOnce = false;
            }
        });


        layout_courses.setOnClickListener(v -> {
            if (current != 2) {
                viewPager2.setCurrentItem(2, false);
                current = 2;
                setDrawableIcon(2);
                pressedBackOnce = false;
            }
        });

        layout_webinar.setOnClickListener(v -> {
            if (current != 3) {
                viewPager2.setCurrentItem(3, false);
                current = 3;
                setDrawableIcon(3);
                pressedBackOnce = false;
            }
        });

        layout_settings.setOnClickListener(v -> {
            if (current != 4) {
                viewPager2.setCurrentItem(4, false);
                current = 4;
                setDrawableIcon(4);
                pressedBackOnce = false;
            }
        });

    }

    @Override
    public void onBackPressed() {
        if (pressedBackOnce) {
            overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
            finish();
        } else {
            viewPager2.setCurrentItem(0, false);
            current = 0;
            setDrawableIcon(0);
            pressedBackOnce = true;
        }
    }

    public void setDrawableIcon(int selected) {

        if (selected == 0) {
            home_ic.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.colorChambray));
        } else {
            home_ic.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.black));
        }

        if (selected == 1) {
            notification_ic.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.colorChambray));
        } else {
            notification_ic.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.black));
        }
        if (selected == 2) {
            courses_ic.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.colorChambray));
        } else {
            courses_ic.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.black));
        }

        if (selected == 3) {
            webinar_ic.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.colorChambray));
        } else {
            webinar_ic.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.black));
        }

        if (selected == 4) {
            menu_ic.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.colorChambray));
        } else {
            menu_ic.setColorFilter(ContextCompat.getColor(HomeActivity.this, R.color.black));
        }

    }



}