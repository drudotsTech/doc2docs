package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.utilities.CustomCookieToast;

public class AddJobActivity extends AppCompatActivity {

    Context context;

    EditText edit_jobTitle, edit_location, edit_description;
    Button btn_Post;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_job);
        context = AddJobActivity.this;

        edit_jobTitle = findViewById(R.id.edit_jobTitle);
        edit_location = findViewById(R.id.edit_location);
        edit_description = findViewById(R.id.edit_description);
        btn_Post = findViewById(R.id.btn_Post);

        btn_Post.setOnClickListener(v -> {
            String jobTitle = edit_jobTitle.getText().toString();
            String location = edit_location.getText().toString();
            String description = edit_description.getText().toString();
            if (jobTitle.isEmpty()){
                CustomCookieToast.showRequiredToast(AddJobActivity.this, "Please enter job title");
            }else if (location.isEmpty()){
                CustomCookieToast.showRequiredToast(AddJobActivity.this, "Please enter job location");
            }else if (description.isEmpty()){
                CustomCookieToast.showRequiredToast(AddJobActivity.this, "Please enter job description");
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }
}