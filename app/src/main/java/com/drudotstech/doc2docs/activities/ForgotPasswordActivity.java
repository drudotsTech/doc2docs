package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.OtpResponseModel;
import com.drudotstech.doc2docs.network.GetDataService;
import com.drudotstech.doc2docs.network.RetrofitClientInstance;
import com.drudotstech.doc2docs.utilities.CustomCookieToast;
import com.drudotstech.doc2docs.utilities.Utilities;
import com.wang.avi.AVLoadingIndicatorView;

import org.jetbrains.annotations.NotNull;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    GetDataService service;
    AVLoadingIndicatorView progressLoading;
    View shadow_View;
    Context context;
    Button btn_Next, btn_Back;
    EditText edit_userEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        context = ForgotPasswordActivity.this;
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        progressLoading = findViewById(R.id.loading);
        shadow_View = findViewById(R.id.shadow_View);


        btn_Next = findViewById(R.id.btn_Next);
        edit_userEmail = findViewById(R.id.edit_userEmail);
        btn_Back = findViewById(R.id.btn_Back);

        btn_Next.setOnClickListener(v -> {
            String userEmail = edit_userEmail.getText().toString();
            boolean isValidEmail = Utilities.isValidEmail(userEmail);
            if (userEmail.isEmpty()) {
                CustomCookieToast.showRequiredToast(ForgotPasswordActivity.this,"Please enter your email");
            }
            else if (!isValidEmail){
                CustomCookieToast.showRequiredToast(ForgotPasswordActivity.this,"Please enter valid email");
            }
            else {
                shadow_View.setVisibility(View.VISIBLE);
                progressLoading.setVisibility(View.VISIBLE);
                progressLoading.show();
                sendOtp(userEmail);
            }

        });

        btn_Back.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
        });
    }



    private void sendOtp(String userEmail){
        Call<OtpResponseModel> call = service.sendOtp("email", userEmail);
        call.enqueue(new Callback<OtpResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<OtpResponseModel> call, @NotNull Response<OtpResponseModel> response) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                if (response.isSuccessful()){
                    assert response.body() != null;
                    boolean status  = response.body().isStatus();
                    if (status){
                        Utilities.saveString(context, "otp", String.valueOf(response.body().getOtp()));
                        Utilities.saveString(context, "user_Email", userEmail);
                        Intent i = new Intent(context , OtpActivity.class);
                        startActivity(i);
                        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
                    }
                    else {
                        CustomCookieToast.showFailureToast(ForgotPasswordActivity.this,"Error!", response.body().getAction());
                    }

                }
                else {
                    CustomCookieToast.showFailureToast(ForgotPasswordActivity.this, "Error!", response.message());
                }

            }

            @Override
            public void onFailure(@NotNull Call<OtpResponseModel> call, @NotNull Throwable t) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                CustomCookieToast.showFailureToast(ForgotPasswordActivity.this,"Error!", t.getMessage());
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }
}