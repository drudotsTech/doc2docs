package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.adapters.AllJobsAdapter;
import com.drudotstech.doc2docs.models.EmptyModel;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;

public class JobPortalActivity extends AppCompatActivity {
    Context context;

    RecyclerView recyclerView_AllJobs;
    List<EmptyModel> allJobsList;
    AllJobsAdapter allJobsAdapter;


    ImageView menu_ic;
    BottomSheetDialog bottomSheetDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_portal);
        context = JobPortalActivity.this;

        recyclerView_AllJobs = findViewById(R.id.recyclerView_AllJobs);
        menu_ic = findViewById(R.id.menu_ic);

        allJobsList = new ArrayList<>();

        for (int i=0; i<10; i++){
            allJobsList.add(new EmptyModel());
        }

        showAllJobsRecyclerView(recyclerView_AllJobs, allJobsList);

        menu_ic.setOnClickListener(v -> {
            bottomSheetDialog = new BottomSheetDialog(JobPortalActivity.this, R.style.BottomSheetTheme);
            View sheetView = LayoutInflater.from(context).inflate(R.layout.job_portal_bottom_sheet, v.findViewById(R.id.job_portal_bottom_sheet));
            LinearLayout btn_myJobs = sheetView.findViewById(R.id.btn_myJobs);
            LinearLayout btn_savedJobs = sheetView.findViewById(R.id.btn_savedJobs);
            LinearLayout btn_postAJob = sheetView.findViewById(R.id.btn_postAJob);

            bottomSheetDialog.setDismissWithAnimation(true);

            btn_myJobs.setOnClickListener(v1 -> {
                bottomSheetDialog.dismiss();
                Intent myJobsIntent = new Intent(context, MyJobsActivity.class);
                startActivity(myJobsIntent);
                overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);

            });

            btn_savedJobs.setOnClickListener(v1 -> {
                bottomSheetDialog.dismiss();
                Intent myJobsIntent = new Intent(context, SavedJobsActivity.class);
                startActivity(myJobsIntent);
                overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
            });

            btn_postAJob.setOnClickListener(v1 -> {
                bottomSheetDialog.dismiss();
                Intent myJobsIntent = new Intent(context, AddJobActivity.class);
                startActivity(myJobsIntent);
                overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
            });
            bottomSheetDialog.setContentView(sheetView);
            bottomSheetDialog.show();
        });
    }

    private void showAllJobsRecyclerView(RecyclerView recyclerView, List<EmptyModel> allJobsList) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        allJobsAdapter = new AllJobsAdapter(context, allJobsList);
        recyclerView.setAdapter(allJobsAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }
}