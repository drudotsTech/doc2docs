package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.os.Bundle;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.adapters.MyJobsAdapter;
import com.drudotstech.doc2docs.models.EmptyModel;

import java.util.ArrayList;
import java.util.List;

public class MyJobsActivity extends AppCompatActivity {

    Context context;

    RecyclerView recyclerView_myJobs;
    List<EmptyModel> myJobsList;
    MyJobsAdapter myJobsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_jobs);

        context = MyJobsActivity.this;

        recyclerView_myJobs = findViewById(R.id.recyclerView_myJobs);

        myJobsList = new ArrayList<>();

        for (int i=0; i<10; i++){
            myJobsList.add(new EmptyModel());
        }

        showMyJobsRecyclerView(recyclerView_myJobs, myJobsList);
    }

    private void showMyJobsRecyclerView(RecyclerView recyclerView, List<EmptyModel> myJobsList) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        myJobsAdapter = new MyJobsAdapter(context, myJobsList);
        recyclerView.setAdapter(myJobsAdapter);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }
}