package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.adapters.AllSurveyAdapter;
import com.drudotstech.doc2docs.models.EmptyModel;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import java.util.ArrayList;
import java.util.List;

public class ClinicalStudyActivity extends AppCompatActivity {
    Context context;

    RecyclerView recyclerView_Surveys;
    List<EmptyModel> listItems;
    AllSurveyAdapter allSurveyAdapter;

    ImageView menu_ic;
    BottomSheetDialog bottomSheetDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinical_study);

        context = ClinicalStudyActivity.this;

        recyclerView_Surveys = findViewById(R.id.recyclerView_Surveys);
        menu_ic = findViewById(R.id.menu_ic);
        listItems = new ArrayList<>();

        for (int i =0; i< 10; i++){
            listItems.add(new EmptyModel());
        }

        showAllSurveyRecyclerView(recyclerView_Surveys, listItems);


        menu_ic.setOnClickListener(v -> {
            bottomSheetDialog = new BottomSheetDialog(ClinicalStudyActivity.this, R.style.BottomSheetTheme);
            View sheetView = LayoutInflater.from(context).inflate(R.layout.bottom_sheet_clinical_study, v.findViewById(R.id.bottom_sheet_clinical_study));
            LinearLayout btn_myClinicalStudy = sheetView.findViewById(R.id.btn_myClinicalStudy);

            btn_myClinicalStudy.setOnClickListener(v1 -> {
                bottomSheetDialog.dismiss();
                Intent mySurveyIntent = new Intent(context, MyClinicalStudyActivity.class);
                startActivity(mySurveyIntent);
                overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
            });

            bottomSheetDialog.setDismissWithAnimation(true);
            bottomSheetDialog.setContentView(sheetView);
            bottomSheetDialog.show();
        });
    }


    private void showAllSurveyRecyclerView(RecyclerView recyclerView, List<EmptyModel> allSurveyList) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        allSurveyAdapter = new AllSurveyAdapter(context, allSurveyList);
        recyclerView.setAdapter(allSurveyAdapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }
}