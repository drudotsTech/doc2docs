package com.drudotstech.doc2docs.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.models.CountriesModel;
import com.drudotstech.doc2docs.models.VerifyResponseModel;
import com.drudotstech.doc2docs.network.GetDataService;
import com.drudotstech.doc2docs.network.RetrofitClientInstance;
import com.drudotstech.doc2docs.utilities.CustomCookieToast;
import com.drudotstech.doc2docs.utilities.Utilities;
import com.hbb20.CountryCodePicker;
import com.wang.avi.AVLoadingIndicatorView;
import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpActivity extends AppCompatActivity {

    EditText edit_userName, edit_userEmail, edit_phoneNumber, edit_userPassword, edit_confirmPassword;
    Button btn_Next;
    CountryCodePicker countryCodePicker;


    GetDataService service;
    AVLoadingIndicatorView progressLoading;
    View shadow_View;
    Context context;
    boolean done = false;

    ImageView showHide_ic, confirm_showHide_ic;
    boolean isPasswordShown, confirmPasswordShown= false;

    Button btn_Back;

    String selectedCountryCode;
    List<CountriesModel> modelList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        context  = SignUpActivity.this;
        service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        progressLoading = findViewById(R.id.loading);
        shadow_View = findViewById(R.id.shadow_View);

        edit_userName = findViewById(R.id.edit_userName);
        edit_userEmail = findViewById(R.id.edit_userEmail);
        edit_phoneNumber = findViewById(R.id.edit_phoneNumber);
        edit_userPassword = findViewById(R.id.edit_userPassword);
        edit_confirmPassword = findViewById(R.id.edit_confirmPassword);
        btn_Next = findViewById(R.id.btn_Next);
        countryCodePicker = findViewById(R.id.countryCodePicker);
        showHide_ic = findViewById(R.id.showHide_ic);
        confirm_showHide_ic = findViewById(R.id.confirm_showHide_ic);
        btn_Back = findViewById(R.id.btn_Back);

        modelList = new ArrayList<>();

        setUserNameTextWatcher();

        try {
            JSONArray m_jArry = new JSONArray(Utilities.loadJSONFromAsset(SignUpActivity.this));
            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                String name = jo_inside.optString("name");
                String code = jo_inside.optString("code");
                String flag = jo_inside.optString("flag");
                String format = jo_inside.optString("format");
                String dial_code = jo_inside.optString("dial_code");
                String digit1 = jo_inside.optString("digit1");
                String digit2 = jo_inside.optString("digit2");

                modelList.add(new CountriesModel(name, code, format, flag, dial_code, digit1, digit2));
            }
        } catch (JSONException e) {
            e.printStackTrace();
            Log.d("tafsvdjdvjvdf", e.getMessage());
        }

        selectedCountryCode = countryCodePicker.getTextView_selectedCountry().getText().toString();
        for (CountriesModel countriesModel : modelList){
            String dialCode = countriesModel.getDial_code();
            if (dialCode.equals(selectedCountryCode)){
                int maxLength = Integer.parseInt(countriesModel.getDigit1());
                String format = countriesModel.getFormat();
                edit_phoneNumber.setHint(format);
                edit_phoneNumber.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
            }
        }

        countryCodePicker.setOnCountryChangeListener(() -> {
            selectedCountryCode = countryCodePicker.getTextView_selectedCountry().getText().toString();
            for (CountriesModel countriesModel : modelList){
                String dialCode = countriesModel.getDial_code();
                if (dialCode.equals(selectedCountryCode)){
                    int maxLength = Integer.parseInt(countriesModel.getDigit1());
                    String format = countriesModel.getFormat();
                    edit_phoneNumber.setHint(format);
                    edit_phoneNumber.setFilters(new InputFilter[] {new InputFilter.LengthFilter(maxLength)});
                }
            }
        });

        btn_Next.setOnClickListener(view -> {

            String userEmail = edit_userEmail.getText().toString();
            String userName = edit_userName.getText().toString();
            String userPassword = edit_userPassword.getText().toString();
            String confirmPassword = edit_confirmPassword.getText().toString();
            String phone = edit_phoneNumber.getText().toString();
            String userPhone = countryCodePicker.getSelectedCountryCode() + phone;
            String userCountryCode = countryCodePicker.getSelectedCountryCode();

            boolean isValidEmail = Utilities.isValidEmail(userEmail);

            if (userName.isEmpty())
            {
                CustomCookieToast.showRequiredToast(SignUpActivity.this,"Please enter your username");
            }
            else if (userEmail.isEmpty())
            {
                CustomCookieToast.showRequiredToast(SignUpActivity.this,"Please enter your email");
            }
            else if (! isValidEmail){
                CustomCookieToast.showRequiredToast(SignUpActivity.this,"Please enter valid email");

            }
            else if (phone.isEmpty()){
                CustomCookieToast.showRequiredToast(SignUpActivity.this,"Please enter your phone number");
            }
            else if (userPassword.isEmpty())
            {
                CustomCookieToast.showRequiredToast(SignUpActivity.this,"Please enter your password");

            }
            else if (userPassword.length() < 6){
                CustomCookieToast.showRequiredToast(SignUpActivity.this,"Please enter 6 digit password");
            }
            else if (confirmPassword.isEmpty()){
                CustomCookieToast.showRequiredToast(SignUpActivity.this,"Please enter confirm password");

            }
            else if (! userPassword.equals(confirmPassword)){
                CustomCookieToast.showRequiredToast(SignUpActivity.this,"Cannot match password");
            }
            else {
                shadow_View.setVisibility(View.VISIBLE);
                progressLoading.setVisibility(View.VISIBLE);
                progressLoading.show();
                confirmUserEmailAndName(userEmail, userName, userPassword, userPhone, userCountryCode);
            }



        });



        showHide_ic.setOnClickListener(v -> {
            if (!isPasswordShown){
                showHide_ic.setImageResource(R.drawable.show_ic);
                isPasswordShown = true;
                edit_userPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else {
                showHide_ic.setImageResource(R.drawable.hide_ic);
                isPasswordShown = false;
                edit_userPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });

        confirm_showHide_ic.setOnClickListener(v -> {
            if (!confirmPasswordShown){
                confirm_showHide_ic.setImageResource(R.drawable.show_ic);
                confirmPasswordShown = true;
                edit_confirmPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            }
            else {
                confirm_showHide_ic.setImageResource(R.drawable.hide_ic);
                confirmPasswordShown = false;
                edit_confirmPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        });

        btn_Back.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
        });
    }


    private void confirmUserEmailAndName(String userEmail, String userName, String userPassword, String userPhone, String userCountryCode){

        Call<VerifyResponseModel> call = service.verifyUserEmail("verify", userEmail, userName, userPhone);
        call.enqueue(new Callback<VerifyResponseModel>() {
            @Override
            public void onResponse(@NotNull Call<VerifyResponseModel> call, @NotNull Response<VerifyResponseModel> response) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                if (response.isSuccessful()){
                    assert response.body() != null;
                    boolean emailStatus  = response.body().isStatus();
                    if (emailStatus){
                        Utilities.saveString(context, "user_Name", userName);
                        Utilities.saveString(context, "user_Email", userEmail);
                        Utilities.saveString(context, "user_Password", userPassword);
                        Utilities.saveString(context, "user_Phone", userPhone);
                        Utilities.saveString(context, "user_countryCode", userCountryCode);
                        Utilities.saveString(context, "isSocialSignUp", "simple");
                        startActivity(new Intent(SignUpActivity.this , PersonalInformationActivity.class));
                        overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
                    }
                    else {
                       String errorType = response.body().getType();
                        switch (errorType) {
                            case "user_name":
                            case "all":
                                CustomCookieToast.showFailureToast(SignUpActivity.this, "Username Error!", "Your username already exist please enter unique username");
                                break;
                            case "email":
                                CustomCookieToast.showFailureToast(SignUpActivity.this, "Email Error!", "Your email already exist please enter other email");
                                break;
                            case "phone":
                                CustomCookieToast.showFailureToast(SignUpActivity.this, "Phone number Error!", "Your phone number already exist please enter other phone");
                                break;
                        }
                    }
                }
                else {
                    CustomCookieToast.showFailureToast(SignUpActivity.this,"Error!", response.message());
                }

            }

            @Override
            public void onFailure(@NotNull Call<VerifyResponseModel> call, @NotNull Throwable t) {
                shadow_View.setVisibility(View.GONE);
                progressLoading.hide();
                CustomCookieToast.showFailureToast(SignUpActivity.this,"Error!", t.getMessage());
            }
        });
    }


    private void setUserNameTextWatcher() {
        edit_userName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!done) {
                    String validUsername = getValidUserName(s.toString());
                    edit_userName.setText(validUsername);
                    edit_userName.setSelection(validUsername.length());
                } else {
                    done = false;
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private String getValidUserName(String name) {
        done = true;
        for (int i = 0; i < name.length(); i++) {
            char ch = name.charAt(i);
            if ((ch < 'a' || ch > 'z') && (ch < '0' || ch > '9') && ch != '_' && ch != '.') {
                name = name.substring(0, i) + name.substring(i + 1);
            }
        }
        return name;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_enter, R.anim.activity_exit);
    }



}