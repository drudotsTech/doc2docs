package com.drudotstech.doc2docs.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.activities.ClinicalStudyActivity;
import com.drudotstech.doc2docs.activities.JobPortalActivity;


public class MenuFragment extends Fragment {

    RelativeLayout btn_jobs, btn_clinicalStudy;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        btn_jobs = view.findViewById(R.id.btn_jobs);
        btn_clinicalStudy = view.findViewById(R.id.btn_clinicalStudy);

        btn_jobs.setOnClickListener(v -> {
            Intent jobsIntent = new Intent(requireActivity(), JobPortalActivity.class);
            startActivity(jobsIntent);
            requireActivity().overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
        });

        btn_clinicalStudy.setOnClickListener(v -> {
            Intent jobsIntent = new Intent(requireActivity(), ClinicalStudyActivity.class);
            startActivity(jobsIntent);
            requireActivity().overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);
        });

        return view;
    }
}