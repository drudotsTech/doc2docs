package com.drudotstech.doc2docs.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.drudotstech.doc2docs.R;
import com.drudotstech.doc2docs.adapters.HomeAdapter;
import com.drudotstech.doc2docs.models.EmptyModel;
import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    RecyclerView recyclerView_Posts;
    HomeAdapter homeAdapter;
    List<EmptyModel> listItems;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        context = requireActivity();

        recyclerView_Posts = view.findViewById(R.id.recyclerView_Posts);

        listItems = new ArrayList<>();


        listItems.add(new EmptyModel("post"));
        listItems.add(new EmptyModel("webinar"));
        listItems.add(new EmptyModel("survey"));
        listItems.add(new EmptyModel("post"));
        listItems.add(new EmptyModel("webinar"));
        listItems.add(new EmptyModel("survey"));
        listItems.add(new EmptyModel("post"));
        listItems.add(new EmptyModel("webinar"));
        listItems.add(new EmptyModel("survey"));
        listItems.add(new EmptyModel("post"));
        listItems.add(new EmptyModel("webinar"));
        listItems.add(new EmptyModel("survey"));
        listItems.add(new EmptyModel("survey"));
        listItems.add(new EmptyModel("post"));
        listItems.add(new EmptyModel("webinar"));
        listItems.add(new EmptyModel("survey"));

        showMainRecyclerView(recyclerView_Posts, listItems);
        return view;
    }

    private void showMainRecyclerView(RecyclerView recyclerView, List<EmptyModel> postList) {
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        homeAdapter = new HomeAdapter(context, postList);
        recyclerView.setAdapter(homeAdapter);
    }


    public void scrollToTop() {
        if (recyclerView_Posts != null && recyclerView_Posts.getChildCount() >= 1) {
            recyclerView_Posts.smoothScrollToPosition(0);
        }
    }
}